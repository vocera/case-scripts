#!/bin/bash

#--------------------------------------------------------------------------------------
# DO NOT MODIFY THIS FILE
#--------------------------------------------------------------------------------------
SCRIPT_NAME=${0##*/}
DATETIME='date'
HOSTNAME='hostname'

#--------------------------------------------------------------------------------------
# Load configuration parmeters
#--------------------------------------------------------------------------------------
CUSTOMER_NAME='***TBC***' # To be completed by installing engineer
SALESFORCE_CASE_NUMBER='***TBC***' # To be completed by installing engineer
JIRA_CASE_NUMBER='CASE-1390' # Original Escalation Engineering CASE id, can be updated if a new escalation is opened
# PRIORITY='HIGH'
# SEND_EMAIL="false"
# HEARBEAT_TIMEOUT=60 #seconds
# EMAIL_TIMEOUT=10    #seconds

# # Email list of receipients, comma separated, no spaces
# EMAIL_ADDRESS=support@vocera.com

# Log File
LOG_FILE=/opt/EXTENSION/log/check_amtelco_sync_interval.log

# Number of archival files to keep
NUM_LOGS_KEEP=10

# Number of bytes that will trigger log file archival
MAX_LOG_SIZE=1000000


#--------------------------------------------------------------------------------------
# Start recording in the log file
#--------------------------------------------------------------------------------------
NOW=$(date '+%Y-%m-%d-%H-%M-%S')
echo "${NOW} INFO: Start ${SCRIPT_NAME} script" >> ${LOG_FILE}

#--------------------------------------------------------------------------------------
# Test log file, move if too big, trim the old ones
#--------------------------------------------------------------------------------------
NOW=$(date '+%Y-%m-%d-%H-%M-%S')
echo "${NOW} INFO: checking log file:${LOG_FILE}" >> ${LOG_FILE}
if [[ -f "${LOG_FILE}" ]]; then

    NOW=$(date '+%Y-%m-%d-%H-%M-%S')
    echo "${NOW} INFO: found log file - get its size and check it..." >> ${LOG_FILE}
    CURRENT_LOG_SIZE=$(stat --printf=%s ${LOG_FILE})

    if [[ ${CURRENT_LOG_SIZE} -ge ${MAX_LOG_SIZE} ]]; then

        NOW=$(date '+%Y-%m-%d-%H-%M-%S')
        echo "${NOW} INFO: log file exceeded:${MAX_LOG_SIZE} - archive it and trim out any over:${NUM_LOGS_KEEP}" >> ${LOG_FILE}
        mv ${LOG_FILE} ${LOG_FILE}-${NOW}
        ls -r1 ${LOG_FILE}-* | tail -n +$((${NUM_LOGS_KEEP}+1)) | xargs rm -f

        echo "${NOW} INFO: log file maintenance completed" >> ${LOG_FILE}
    else
        echo "${NOW} INFO: log file OK, no maitenance required" >> ${LOG_FILE}
    fi

fi

#--------------------------------------------------------------------------------------
# SCRIPT LOGIC GOES HERE
#--------------------------------------------------------------------------------------

# Minimally expect the current log to be present in any Amtelco deployment
if [ -f /opt/EXTENSION/log/AmtelcoInterface.log ]; then
    LOG_LAST_MODIFIED=$(date "+%s" -r /opt/EXTENSION/log/AmtelcoInterface.log)
    NOW_MINUS_10MINUTES=$(date --date '-10 min' "+%s") # Typical poll interval is 5 minutes so checking for no update for 2 times poll interval
    if [ ${LOG_LAST_MODIFIED} -lt ${NOW_MINUS_10MINUTES} ]; then
        NOW=$(date '+%Y-%m-%d-%H-%M-%S')
        echo "${NOW} INFO: Creating thread report (thread-report-${NOW}.txt) in /opt/EXTENSION/log/"  >> ${LOG_FILE}
        /opt/glassfish/glassfish/bin/asadmin generate-jvm-report --type=thread > /opt/EXTENSION/log/thread-report-${NOW}.txt # Per request from Escalation Engineering for CASE-1390 / 00313735 produce a thread report before restarting service
        NOW=$(date '+%Y-%m-%d-%H-%M-%S')
        echo "${NOW} INFO: Thread report created in /opt/EXTENSION/log/"  >> ${LOG_FILE}
        sleep 30s # Give time for thread report to be generated
        WEBKEY=$(cat /opt/EXTENSION/conf/webkey)
        NOW=$(date '+%Y-%m-%d-%H-%M-%S')
        echo "${NOW} INFO: Stopping Amtelco adapter"  >> ${LOG_FILE}
        curl "http://localhost/osgi/sysmgr/stopInterfaceAsync/Amtelco/?webkey=${WEBKEY}"  >> ${LOG_FILE}
        sleep 15s
        NOW=$(date '+%Y-%m-%d-%H-%M-%S')
        echo "${NOW} INFO: Starting Amtelco adapter"  >> ${LOG_FILE}
        curl "http://localhost/osgi/sysmgr/startInterfaceAsync/Amtelco/?webkey=${WEBKEY}"  >> ${LOG_FILE}
    fi
fi