# CASE-1390 Amtelco Adapter Monitor Script

- Original Escalation Engineering Jira ticket: **CASE-1390**
- Created by: **Graham Watts** (Backline Engineer / TSE 4, Vocera Technical Support)
- Created on: **2019-11-15**

## Description

The provided script is in intended to monitor the current Amtelco Adapter log file and if no update to the file modified date has occurred for 2 times the poll interval (typically set to 5 minutes) to then restart the adapter.  The script will create a log file in the regular Vocera Engage log file directory and will rotate logs based on size and a maximum number of log files.

This script is intended to be run via a cron job with an interval period of 1 minute.

## Logging

- Script will generate a glassfish thread report if the Adapter has stopped
- Script will log it's interactions to a rolling log file
- Both log and thread report will be saved to /opt/EXTENSION/log/
